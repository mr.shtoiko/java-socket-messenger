# Java socket messenger
 This is an educational project of a terminal messenger in Java using Java socket technologies. 
---
## The main purpose:
Acquire knowledge of TCP technologies
## Description of the goal
The messenger in the final version should have three ways of launching:
- [ ] through the server (with the possibility of deploying your own server)
- [ ] with receiving the recipient's IP address from the server
- [ ] directly to the recipient's IP address
---
## License
[GNU Affero General Public License v3.0](https://choosealicense.com/licenses/agpl-3.0/)
